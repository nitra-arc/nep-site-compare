/*
 * Compare products
 * Version: 1.0.0 (01/06/2013)
 * Copyright (c) 2013 NitraLabs
 * Requires: jQuery v1.7+
 */
function RemoveIt(addDelCompPath, delCompPath){
    $('#compare_preview #remove').on('click', function () {
        var item = $(this);
        compare.addDelCompare(item.data("id"), addDelCompPath);
        $.ajax({
            type:   'POST',
            url:    delCompPath,
            data:   {
                'product_id': $(this).data("id")
            },
            success: function(){
                updateComparePreview();
            }
        });
        return false;
    });
    
};

