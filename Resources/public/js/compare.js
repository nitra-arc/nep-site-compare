/*
 * Compare products
 * Version: 1.0.0 (01/06/2013)
 * Copyright (c) 2013 NitraLabs
 * Requires: jQuery v1.7+
 */
var compare = (function() {
    var comp = {};

    comp.addDelCompare = function(product_id, path) {
        $.ajax({
            url: path,
            type: 'POST',
            data: {
                product_id: product_id
            },
            dataType: 'json',
            async: false,
            success: function(data) {
                if (data.compare_count != 0) {
                    $('#compare_items_button').addClass('active');
                } else {
                    $('#compare_items_button').removeClass('active');
                }
                if (data.product_added == true) {
                    $('.compare_button_' + product_id).removeClass('add');
                    $('.compare_button_' + product_id).addClass('delete');
                    if ($('#compare_items_button').offset()) {
                        ImageFly(product_id);
                    }
                }
                if (data.product_added == false) {
                    $('.compare_button_' + product_id).removeClass('delete');
                    $('.compare_button_' + product_id).addClass('add');
                }
                $('.compare_button_' + product_id + ' span.text').text(data.compare_text);
                if (data.hide_compare_button) {
                    $('.compare_button_link_' + product_id).css('display', 'none');
                } else {
                    $('.compare_button_link_' + product_id).css('display', 'block');
                }
                $('#compare_items_button #compare_count').text(data.compare_count);
            }
        });
    };


    comp.filter = function(all) {
        if ($('.active tr.first td.compare_visible').length >= 2) {
            var index = 0;
            if (all) {
                $('.filter_different_params').removeClass('active');
                $('.filter_all_params').addClass('active');
            } else {
                $('.filter_all_params').removeClass('active');
                $('.filter_different_params').addClass('active');
            }
            $('.compare_conteiner.active .compare_value').each(function() {
                var hide = true;
                var cVal = null;
                $(this).removeClass('odd');

                if (all) {
                    $(this).show();
                    if (index % 2) {
                        $(this).addClass('odd');
                    }
                    index ++;
                } else {
                    $('.compare', this).each(function() {
                        if (cVal == null) {
                            cVal = $(this).html();
                        } else {
                            if ($(this).html() != cVal) {
                                hide = false;
                            }
                            cVal = $(this).html();
                        }
                    });

                    if (hide) {
                        $(this).hide();
                    } else {
                        if (index % 2) {
                            $(this).addClass('odd');
                        }
                        index ++;
                    }
                }
            });
        }
    };

    comp.hide_diff = function() {
        var index = 0;
        $('.compare_conteiner.active .compare_value').each(function() {
            var cVal = '-';
            var hide = true;
            $(this).removeClass('odd');
            $('.compare', this).each(function() {
                if ($(this).html() != cVal) {
                    hide = false;
                }
            });

            if (hide) {
                $(this).hide();
            } else {
                index ++;
                if (index % 2) {
                    $(this).addClass('odd');
                }
            }
        });
    };

    /**
     * Обновляем состояние стрелок карусели
     * @param {type} categoryId
     * @returns {undefined}
     */
    function updateCarouselButtonsStatus(categoryId) {
        var $list    = $(".compare_conteiner_" + categoryId + " .compare_category_" + categoryId + " .compare_items_list li");

        var $toRight = $('.compare_conteiner_' + categoryId).find('.move_right');
        var $toLeft  = $('.compare_conteiner_' + categoryId).find('.move_left');

        if ($list.last().hasClass('visible')) {
            $toRight.addClass('disable');
        } else {
            $toRight.removeClass('disable');
        }

        if ($list.first().hasClass('visible')) {
            $toLeft.addClass('disable');
        } else {
            $toLeft.removeClass('disable');
        }
    }

    function compare_change(category, numProducts) {
        $(".compare_category").removeClass('active');
        $(".compare_items_list li").removeClass('visible');
        $('.compare_conteiner td.td_item').hide();
        $('.compare_value').show();
        $('td.compare').hide();
        $('.compare_conteiner').removeClass('active');
        $('td.compare').removeClass('compare_visible');
        $('.compare_category_' + category).addClass('active');
        $('.compare_conteiner_' + category).addClass('active');
        var liArr = [];
        if ($(".copy.active.compare_conteiner_" + category).parent('div').css('display') == 'block') {
            liArr = $(".copy.compare_conteiner_" + category + " .compare_category_" + category + " .compare_items_list li");
        } else {
            liArr = $(".basic.compare_conteiner_" + category + " .compare_category_" + category + " .compare_items_list li");
        }
        liArr.each(function(index, value) {
            if (index < numProducts) {
                $(value).addClass('visible');
                var id = $(value).attr('data-relation-id');
                $('.value_' + id).show();
                $('.' + id).show();
                $('td.value_' + id).show();
                $('td.value_' + id).addClass('compare_visible');
            }
            if (($('.filter_different_params').hasClass('active'))) {
                compare.filter(false);
            } else {
                compare.filter(true);
            }
        });
    }

    function sortItems (list, comparator, getSortable) {
        var sort = [].sort;

        getSortable = getSortable || function() { return this; };

        var placements = list.map(function() {
            var sortElement = getSortable.call(this);
            var parentNode = sortElement.parentNode;
            var nextSibling = parentNode.insertBefore(
                document.createTextNode(''),
                sortElement.nextSibling
            );

            return function() {
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }

                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
            };
        });

        return sort.call(list, comparator).each(function(i) {
            placements[i].call(getSortable.call(this));
        });
    };

    comp.delCompare = function(category_id, product_id, path, num_products) {
        $.ajax({
            url: path,
            type: 'POST',
            data: {
                product_id: product_id
            },
            dataType: 'json',
            success: function(data) {
                $('.compare_conteiner .' + product_id).remove();
                $('.compare_params .value_' + product_id).remove();

                num_products = parseInt(num_products) || 2;
                compare_change(category_id, num_products);
                if (data.compare_count == 0) {
                    $('#compare_items_button').removeClass('active');
                    $('.compare_box').html("<div class='no_products'><div class='text'>" + data.no_product_text + "</div><a class='back' onclick='history.go(-1)'> " + data.go_back_text + "</a></div>");

                }
                $('.compare_items_button #compare_count').text(data.compare_count);
                if (($('.compare_conteiner_' + category_id + ' .item').length == 0)) {
                    $('.compare_conteiner_' + category_id).remove();
                    $('.compare_category_' + category_id).remove();
                    $('.compare_category').first().addClass('active');
                    $('.basic.compare_conteiner').first().addClass('active');
                    $('.copy.compare_conteiner').first().addClass('active');
                    $(".compare_items_list li").each(function(index, value) {
                        if (index < num_products) {
                            $(value).addClass('visible');
                            var id = $(value).attr('data-relation-id');
                            $('.value_' + id).show();
                            $('.' + id).show();
                            $('td.value_' + id).show();
                            $('td.value_' + id).addClass('compare_visible');
                        }
                    });
                    if (($('.filter_different_params').hasClass('active'))) {
                        compare.filter(false);
                    } else {
                        compare.filter(true);
                    }
                    fixHead();
                }
                if ((num_products) >= $('.compare_conteiner.active .td_item').length) {
                    $('.compare_conteiner_'+ category_id +' .prev a').addClass('disable');
                    $('.compare_conteiner_'+ category_id +' .next a').addClass('disable');
                }
                $('.compare_category_'+ category_id +' #items_count').text($('.compare_conteiner.active .td_item').length);
            }
        });
    };

    comp.selectCategory = function(category_id, num_products) {
        $('.compare_conteiner .item').show();
        $('td.compare').show();
        num_products = parseInt(num_products) || 2;
        compare_change(category_id, num_products);
        fixHead();
    };

    comp.sort = function(categoryId, numProducts) {
        var copyContainer = ".copy.active.compare_conteiner_"  + categoryId;
        var baseContainer = ".basic.active.compare_conteiner_" + categoryId;
        var copy          = copyContainer + " .compare_category_" + categoryId + " .compare_items_list";
        var base          = baseContainer + " .compare_category_" + categoryId + " .compare_items_list";

        var copyIsBlock   = $(copyContainer).parent('div').css('display') == 'block';

        numProducts = parseInt(numProducts) || 2;
        if (copyIsBlock) {
            var list = copy;
        } else {
            var list = base;
        }
        $(list).sortable({
            cursor: "move",
            change: function() {
                $('.compare_category_' + categoryId).addClass('active');
                $(".compare_items_list li").removeClass('visible');
                $(list + " li").each(function(index, value) {
                    if (index < numProducts) {
                        $(value).addClass('visible');
                    }
                });
            },
            update: function(e) {
                var sorts   = {};
                var visible = [];
                $(this).find('li').each(function(index, value) {
                    var id = $(value).data('relation-id');
                    sorts[id] = index;
                    if (index < numProducts) {
                        visible.push(id);
                        $(value).addClass('visible');
                        $('.value_' + id).show();
                        $('.' + id).show();
                        $('td.value_' + id).show();
                        $('td.value_' + id).addClass('compare_visible');
                    } else {
                        $(value).removeClass('visible');
                    }
                });
                compare.filter(! $('.filter_different_params').hasClass('active'));

                sortItems($('.compare_conteiner_' + categoryId + ' .td_item'), function(a, b) {
                    var idA = $(a).children('div').data('relation-id');
                    var idB = $(b).children('div').data('relation-id');

                    return sorts[idA] > sorts[idB] ? 1 : -1;
                });
                $('.compare_conteiner_' + categoryId + ' .params_row').each(function() {
                    sortItems($(this).children('td.compare'), function(a, b) {
                        var idA = /[a-f\d]{24}/i.exec($(a).attr('class'))[0];
                        var idB = /[a-f\d]{24}/i.exec($(b).attr('class'))[0];

                        return sorts[idA] > sorts[idB] ? 1 : -1;
                    });
                });

                $('.compare_conteiner_' + categoryId + ' .td_item').each(function() {
                    var id = $(this).children('div').data('relation-id');
                    if (visible.indexOf(id) < 0) {
                        $(this)
                            .hide()
                            .addClass('not_visible');
                        $('td.value_' + id)
                            .hide()
                            .removeClass('compare_visible');
                    } else {
                        $(this)
                            .show()
                            .removeClass('not_visible');
                        $('td.value_' + id)
                            .show()
                            .addClass('compare_visible');
                    }
                });
                $('.compare_conteiner_' + categoryId + ' .td_item').removeClass('first_visible');
                $('.compare_conteiner_' + categoryId + ' .td_item:not(.not_visible)').first().addClass('first_visible');

                updateCarouselButtonsStatus(categoryId);
            }
        });
    };

    comp.moveRight = function(countToMove, categoryId, numProducts) {
        numProducts = parseInt(numProducts) || 2;
        var toMove = parseInt(countToMove);
        if ($(".copy.active.compare_conteiner_" + categoryId).parent('div').css('display') == 'block') {
            var liArr = $(".copy.compare_conteiner_" + categoryId + " .compare_category_" + categoryId + " .compare_items_list li");
        } else {
            var liArr = $(".basic.compare_conteiner_" + categoryId + " .compare_category_" + categoryId + " .compare_items_list li");
        }
        var last = false;
        var first = false;
        for (var i = 0; i < liArr.length; i++) {
            var id = $(liArr[i]).attr('data-relation-id');
            if ($('td.' + id).hasClass('first_visible')) {
                $('td.' + $(liArr[i + toMove]).attr('data-relation-id')).addClass('first_visible');
                $('td.' + id).removeClass('first_visible');
                first = i + toMove;
                break;
            }
        }
        if (first + numProducts > liArr.length) {
            first = liArr.length - numProducts;
            $('td.' + $(liArr[ liArr.length - numProducts]).attr('data-relation-id')).addClass('first_visible');
        }
        last = first + numProducts;

        compare.move(first, last, categoryId);
    };

    comp.moveLeft = function(countToMove, categoryId, numProducts) {
        numProducts = parseInt(numProducts) || 2;
        var toMove = parseInt(countToMove);
        if ($(".copy.active.compare_conteiner_" + categoryId).parent('div').css('display') == 'block') {
            var liArr = $(".copy.compare_conteiner_" + categoryId + " .compare_category_" + categoryId + " .compare_items_list li");
        } else {
            var liArr = $(".basic.compare_conteiner_" + categoryId + " .compare_category_" + categoryId + " .compare_items_list li");
        }
        var last = false;
        var first = false;
        for (var i = 0; i < liArr.length; i++) {
            var id = $(liArr[i]).attr('data-relation-id');
            if ($('td.' + id).hasClass('first_visible')) {
                $('td.' + $(liArr[i - toMove]).attr('data-relation-id')).addClass('first_visible');
                $('td.' + id).removeClass('first_visible');
                first = i - toMove;
                break;
            }
        }
        if (first < 0) {
            $('td.' + $(liArr[0]).attr('data-relation-id')).addClass('first_visible');
            first = 0;
        }
        last = first + numProducts;
        compare.move(first, last, categoryId);
    };

    comp.move = function(first, last, categoryId) {
        $(".compare_category").removeClass('active');
        $(".compare_items_list li").removeClass('visible');
        $('.compare_conteiner td.td_item').hide();
        $('.compare_value').show();
        $('td.compare').hide();
        $('.compare_conteiner').removeClass('active');
        $('td.compare').removeClass('compare_visible');
        $('.compare_category_' + categoryId).addClass('active');
        $('.compare_conteiner_' + categoryId).addClass('active');
        var liArr = [];
        if ($(".copy.active.compare_conteiner_" + categoryId).parent('div').css('display') == 'block') {
            liArr = $(".copy.compare_conteiner_" + categoryId + " .compare_category_" + categoryId + " .compare_items_list li");
        } else {
            liArr = $(".basic.compare_conteiner_" + categoryId + " .compare_category_" + categoryId + " .compare_items_list li");
        }
        liArr.each(function(index, value) {
            if (index >= first && index < last) {
                $(value).addClass('visible');
                var id = $(value).attr('data-relation-id');
                $('.value_' + id).show();
                $('.' + id).show();
                $('td.value_' + id).show();
                $('td.value_' + id).addClass('compare_visible');
            }
            if (($('.filter_different_params').hasClass('active'))) {
                compare.filter(false);
            } else {
                compare.filter(true);
            }
        });

        updateCarouselButtonsStatus(categoryId);
    };

    function fixHead() {
        $('.basic.compare_conteiner').each(function(index) {
            if ($(this).hasClass('active')) {
                var thead = $(this).children('thead')[0];
                if ($(thead).hasClass('fixed_head')) {
                    if ($(document).scrollTop() >= $(thead).position().top) {
                        $('#fixed_head_' + index).show();
                    } else {
                        $('#fixed_head_' + index).hide();
                    }
                }
            }
        });
    }

    function ImageFly(rel) {

        var image = $('#list_image_' + rel);
        var compare = $('#compare_items_button');
        of_top = image.offset().top;
        of_left = image.offset().left;
        of_top_cart = compare.offset().top;
        of_left_cart = compare.offset().left;

        params = {
            top: of_top_cart,
            left: of_left_cart,
            opacity: 0.0,
            width: 50,
            heigth: 50
        };

        $('#list_image_' + rel).clone().css({'position': 'absolute', 'z-index': '9999', top: +of_top + 'px', left: +of_left + 'px'}).appendTo('body')
                .animate(params, 1200, false, function() {
                    $(this).remove();
                });
    }
    ;

    $(document).ready(function() {
        $('.compare_conteiner').each(function(index, value) {
            var thead = $(value).children('thead')[0];
            if ($(thead).hasClass('fixed_head')) {
                var div = '<div id="fixed_head_' + index + '" class="fixed_head_container"></div>';
                $('.compare_page').append(div);
                $(this).clone().appendTo($('#fixed_head_' + index));
                $('#fixed_head_' + index).hide();
                $('#fixed_head_' + index).children('.compare_conteiner').addClass('copy');
                $('#fixed_head_' + index).children('.compare_conteiner').removeClass('basic');
                $('#fixed_head_' + index + ' tbody').remove();
            }
        });
        document.onscroll = fixHead;
    });

    return comp;
})();