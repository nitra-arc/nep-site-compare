<?php

namespace Nitra\CompareBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Nitra\CompareBundle\DependencyInjection\CompilerPass\TwigGlobalsCompilerPass;

class NitraCompareBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new TwigGlobalsCompilerPass());
    }
}