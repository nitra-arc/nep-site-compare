<?php

namespace Nitra\CompareBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class TwigGlobalsCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $twigDefinition = $container->getDefinition('twig');

        foreach ($container->getParameter('nitra.compare') as $key => $value) {
            $twigDefinition->addMethodCall('addGlobal', array(
                $key,
                $value,
            ));
        }
    }
}