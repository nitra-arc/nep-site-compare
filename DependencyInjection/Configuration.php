<?php

namespace Nitra\CompareBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $builder
            ->root('nitra_compare')
            ->addDefaultsIfNotSet()
            ->children()
                ->integerNode('compareCountVisibleProd')
                    ->defaultValue(3)
                ->end()
                ->booleanNode('compareDraggableList')
                    ->defaultTrue()
                ->end()
                ->booleanNode('compareFixedHead')
                    ->defaultFalse()
                ->end()
                ->booleanNode('useComparePreview')
                    ->defaultFalse()
                ->end()
                ->integerNode('comparePreviewCountProd')
                    ->defaultValue(4)
                ->end()
            ->end();

        return $builder;
    }
}