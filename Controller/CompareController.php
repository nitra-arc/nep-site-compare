<?php

namespace Nitra\CompareBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

class CompareController extends NitraController
{
    /**
     * Добавление/удаление товаров для сравнения из сессии
     *
     * @Route("/add-delete-compare", name="add_del_compare")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addDelCompareAction(Request $request)
    {
        $productId = $request->request->get('product_id');

        $session = $request->getSession();
        // массив товаров к сравнению
        $compare            = $session->get('compare');
        $compareText        = $this->getTranslator()
            ->trans('compare.actions.add', array(), "NitraCompareBundle");
        $hideCompareButton  = true;
        $productAdded       = false;
        if ($productId) {
            if (isset($compare[$productId])) {
                unset($compare[$productId]);
            } else {
                $compare[$productId]    = new \MongoId($productId);
                $compareText            = $this->getTranslator()->trans('compare.actions.delete', array(), "NitraCompareBundle");
                $hideCompareButton      = false;
                $productAdded           = true;
                $session->set('last_compare_prod', $productId);
            }
        }
        $session->set('compare', $compare);

        return new JsonResponse(array(
            'hide_compare_button' => $hideCompareButton,
            'product_added'       => $productAdded,
            'compare_text'        => $compareText,
            'compare_count'       => count($compare),
        ));
    }

    /**
     * Удаление товаров на странице сравнения из сессии
     *
     * @Route("/delete-compare", name="del_compare")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function delCompareAction(Request $request)
    {
        $productId = $request->request->get('product_id');

        $session   = $request->getSession();
        // массив товаров к сравнению
        $compare   = $session->get('compare');
        $last_compare_prod   = $session->get('last_compare_prod');
        if ($productId == $last_compare_prod) {
            $session->remove('last_compare_prod');
        }
        if ($productId) {
            if (isset($compare[$productId])) {
                unset($compare[$productId]);
            }
            if ($compare) {
                $session->set('compare', $compare);
            } else {
                $session->remove('compare');
            }
        }

        return new JsonResponse(array(
            'compare_count'   => count($compare),
            'no_product_text' => $this->getTranslator()
                ->trans('compare.noproducts', array(), "NitraCompareBundle"),
            'go_back_text'    => $this->getTranslator()
                ->trans('compare.returnback', array(), "NitraCompareBundle"),
        ));
    }

    /**
     * Краткий список сравнения
     *
     * @Route("/compare_preview", name="compare_preview")
     * @Template("NitraCompareBundle:Compare:comparePreviewProducts.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function comparePreviewProductsAction(Request $request)
    {
        $session  = $request->getSession();
        $compare  = $session->get('compare');
        $products = $compare
            ? $this->getComparisonProducts($compare)
            : array();

        return array(
            'products'  => $products,
        );
    }

    /**
     * Сравнение товаров
     *
     * @Route("/compare", name="compare_page")
     * @Template("NitraCompareBundle:Compare:comparePage.html.twig")
     *
     * @param Request $request
     *
     * @return array Template context
     */
    public function compareAction(Request $request)
    {
        $session         = $request->getSession();
        $compare         = $session->get('compare');
        $categories      = array();

        if ($compare) {
            $products   = $this->getComparisonProducts($compare);
            $categories = $this->groupComparisonProducts($products, $session->get('last_compare_prod'));
        }

        return array(
            'categoriesProducts'    => $categories,
        );
    }

    /**
     * Get comparison products
     *
     * @param array $compare
     *
     * @return \Nitra\ProductBundle\Document\Product[]
     */
    protected function getComparisonProducts($compare)
    {
        return $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getDefaultQb()
            ->field('id')->in(array_values($compare))
            ->getQuery()->execute();
    }

    /**
     * Group comparison products by category
     *
     * @param \Nitra\ProductBundle\Document\Product[] $products
     * @param string                                  $lastCompareProd
     *
     * @return array Grouped data
     */
    protected function groupComparisonProducts($products, $lastCompareProd)
    {
        $sorts         = array();
        $result        = array();
        $firstCategory = '';
        foreach ($products as $product) {
            $category = $product->getModel()->getCategory();
            if ($product->getId() == $lastCompareProd) {
                $firstCategory = $category->getId();
            }
            if (!array_key_exists($category->getId(), $result)) {
                $result[$category->getId()] = array(
                    'categoryName'   => $category->getName(),
                    'products'       => array(),
                    'categoryParams' => array(),
                );
            }
            $result[$category->getId()]['products'][$product->getId()] = array(
                'obj'        => $product,
                'parameters' => $this->formatProductParameters($product, $result[$category->getId()]['categoryParams']),
            );
        }
        foreach (array_keys($result) as $categoryId) {
            $sorts[] = ($categoryId == $firstCategory) ? 0 : 1;
        }
        array_multisort($sorts, $result);

        return $result;
    }

    /**
     * Format product parameters
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     * @param array                                 $parameters
     *
     * @return array
     */
    protected function formatProductParameters($product, &$parameters)
    {
        $result = array();
        foreach ($product->getParameters() as $parameter) {
            if (!array_key_exists($parameter->getParameter()->getGroup(), $parameters)) {
                $parameters[$parameter->getParameter()->getGroup()] = array();
            }
            $parameters[$parameter->getParameter()->getGroup()][$parameter->getParameter()->getId()] = $parameter->getParameter();
            $paramValues = array();
            foreach ($parameter->getValues() as $pv) {
                $paramValues[] = $pv->getName();
            }
            $result[$parameter->getParameter()->getId()] = implode(', ', $paramValues);
        }

        return $result;
    }
    
    /**
     * Actions для добавления товара к сравнению
     *
     * @Template("NitraCompareBundle:Compare:compareButton.html.twig")
     * @Cache(smaxage="0", public="true", expires="3600")
     * @param string $productId
     *
     * @return array
     */
    public function getCompareButtonAction($productId)
    {
        return array(
            'productId' => $productId,
        );
    }

}