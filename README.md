# SlideshowBundle

## Подключение в проект
* composer.json:

```json
{
    ...   
    "require": {
        ...
        "e-commerce-site/comparebundle": "dev-master",
        ...
    }
    ...
}
```

## Подключение маршрутизации

```yaml
    # app/config/routing.yml
    # ...
    nitra_compare:
        resource: "@NitraCompareBundle/Resources/config/routing.yml"
        prefix:   /
    # ...
```

## Добавление в AppKernel.php

```php
    # app/AppKernel.php

    # ...
    public function registerBundles()
    {
        $bundles = array(
            # ...
            new Nitra\CompareBundle\NitraCompareBundle(),
        );
    }
    # ...
```

## Конфигурация по умолчанию:
```yaml
    # app/config/config.yml

    # ...
    nitra_compare:
        compareDraggableList:       false
        compareFixedHead:           true
        compareCountVisibleProd:    4
        useComparePreview:          false
        comparePreviewCountProd:    4
    # ...
```

### Описание настроек модуля сравнений

Доступные настройки для сравнений (глобальные переменные): 

* compareDraggableList - перемещение сравниваемых товаров boolean (true / false), по умолчанию true
* compareFixedHead - фиксированный список товаров boolean (true / false), по умолчанию true
* compareCountVisibleProd - число видимых товаров на странице сравнений integer
* useComparePreview - краткий список сравниваемых товаров (true / false), по умолчанию false
* comparePreviewCountProd - число товаров для краткого списка сравниваемых товаров, по умолчанию false, если передавать integer значение, будет срабатывать карусель

## Использование в проекте

### Подключение скрипта

```twig
    # app/Resources/views/base.html.twig
    
    # ...
    {% block javascript_head %}
        {{ parent() }}
        {% javascripts '@NitraCompareBundle/Resources/public/js/compare.js' %}
            <script src="{{ asset_url }}"></script>
        {% endjavascripts %}
        # ...
    {% endblock javascript_head %}
    # ...
```

Если используется краткий список сравнений необходимо также подключить скрипт '@NitraCompareBundle/Resources/public/js/compare.preview.js'


### Пример добавления в шаблон (счетчик для товаров сравнения - выводится на всех страницах)

```twig
    # app/Resources/views/base.html.twig

    # ...
    {% block main_compare %}
        {{ include('NitraCompareBundle:Compare:compareButtonCount.html.twig') }}
    {% endblock main_compare %}
    # ...
```